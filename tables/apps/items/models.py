# -*- coding: utf-8 -*-

from django.db import models

class Items(models.Model):
    name = models.TextField(max_length=50, verbose_name='Name')

    def __unicode__(self):
        return '%s' % (self.name)

    class Meta:
        verbose_name_plural = 'Names'
        verbose_name = 'Name'
