# -*- coding: utf-8 -*-
from django.contrib import admin
 
from tables.apps.items.models import Items
 
class ItemsAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ('name',)
 
admin.site.register(Items, ItemsAdmin)
