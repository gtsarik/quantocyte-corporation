# -*- coding: utf-8 -*-

from django.shortcuts import render
from tables.apps.items.models import Items
from lxml import etree
from django.conf import settings

def index(request):
    main_list = Items.objects.all()
    file_xml = '/sampledata/items.xml'
    fullpath = settings.STATICFILES_DIRS[0] + file_xml
    table_id = []
    table_name = []
    key_val = {}

    for k in main_list:
        table_id.append(int(k.id))
        table_name.append(str(k.name))
        key_val = zip(table_id, table_name)
    root = etree.Element("Items")    

    for k in key_val:
        root1 = etree.Element("Item", ItemID="%d" % k[0])
        etree.SubElement(root1, "CategoryID").text = "%d" % k[0]
        etree.SubElement(root1, "Name").text = str(k[1])
        app_window = etree.tostring(root1)
        root.append(etree.XML(app_window))
    handle = etree.tostring(root, pretty_print=True, encoding='utf-8', xml_declaration=True)
    applic = open(fullpath, "w")
    applic.writelines(handle)
    applic.close()
    title = 'Items'
    context = {'title': title}
 
    return render(request, 'items/index.html', context)
